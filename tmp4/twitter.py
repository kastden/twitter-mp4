#!/usr/bin/env python3

import json
from urllib.parse import urlparse

import requests
from bs4 import BeautifulSoup as bs4


class Twitter(object):
    _headers = {
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1'
    }

    def __init__(self):
        self.session = requests.Session()

    def get_json(self, url):
        o = urlparse(url)
        if not o.netloc.endswith("twitter.com"):
            return False

        if "/status/" not in o.path:
            return False

        r = self.session.get(url, headers=self._headers)
        try:
            r.raise_for_status()
        except:
            return False

        data = r.text
        soup = bs4(data, "html.parser")

        init_data = soup.find('script', id="init-data")
        return json.loads(init_data.string)

    def find_video(self, post_url):
        data = self.get_json(post_url)
        tweet = data['state']['tweetDetail']['tweet']

        media = tweet.get('inlineMedia', None)

        if not media:
            return None

        media_details = media.get('mediaDetails', None)
        if not media_details:
            return None

        if media_details['contentType'] == 'video/mp4':
            return media_details['videoUrl']

if __name__ == "__main__":
    t = Twitter()
    print(t.find_video("https://twitter.com/yahho_sahho/status/763751439210459136"))
