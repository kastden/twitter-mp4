#!/usr/bin/env python3

from urllib.parse import urlparse

from flask import Flask, redirect, request, abort

from tmp4.twitter import Twitter


app = Flask(__name__)

t = Twitter()


@app.route('/')
def get_video():
    params = request.args
    if "url" not in params:
        return abort(404)

    post_url = params.get('url')

    o = urlparse(post_url)
    if not o.netloc.endswith("twitter.com"):
        return abort(403)

    if "/status/" not in o.path:
        return abort(403)

    mp4_url = t.find_video(post_url)
    return redirect(mp4_url, 302)
