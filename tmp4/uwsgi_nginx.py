#!/usr/bin/env python3
'''Set nginx+uwsgi specific options for the app.'''

from tmp4 import app

app.debug = True
