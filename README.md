### Get the .mp4-link to a video on twitter.com  
Includes a Flask app for use with bookmarklets.

#### Example bookmarklet
	javascript:window.location="https://HOSTNAME.TLD/?url="+window.location.href;